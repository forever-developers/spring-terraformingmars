package prov1.utils;

import org.springframework.http.HttpStatus;

public class PresentacioException extends RuntimeException {

	private HttpStatus httpStatus;

	public PresentacioException(String message, HttpStatus httpStatus) {
		super(message);
		this.httpStatus = httpStatus;
	}

	public PresentacioException(String message, int statusCode) {
		super(message);
		this.httpStatus = HttpStatus.valueOf(statusCode);
	}

	public HttpStatus getHttpStatus() {
		return httpStatus;
	}

}
