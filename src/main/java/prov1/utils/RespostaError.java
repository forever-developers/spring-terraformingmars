package prov1.utils;

import java.io.Serializable;

public class RespostaError implements Serializable {

	private String error;

	public RespostaError(String error) {
		this.error = error;
	}

	public String getError() {
		return error;
	}

}
