package prov1.utils;

public class ReturnOfJedy {

	private String estat;

	public ReturnOfJedy() {
		super();
	}

	public ReturnOfJedy(String estat) {
		super();
		this.estat = estat;
	}

	public String getEstat() {
		return estat;
	}

	public void setEstat(String estat) {
		this.estat = estat;
	}

}
