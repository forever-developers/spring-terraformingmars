package prov1.entity;

import java.io.Serializable;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;

@Entity
@Table(name = "GameWinner")
public class GameWinner implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idGameWinner", updatable = false, insertable = false)
	private int idGameWinner;

	@ManyToOne
	@JoinColumn(name = "idPlayer")
	@JsonManagedReference
	private Players players;

	@OneToOne(cascade = CascadeType.PERSIST, mappedBy = "gameWinner")
	@JsonBackReference
	private Games games;

	public GameWinner() {
		super();
	}

	public GameWinner(Players players, Games games) {
		super();
		this.players = players;
		this.games = games;
	}

	public GameWinner(int idGameWinner, Players players, Games games) {
		this.idGameWinner = idGameWinner;
		this.players = players;
		this.games = games;
	}

	public Players getPlayers() {
		return players;
	}

	public void setPlayers(Players players) {
		this.players = players;
	}

	public Games getGames() {
		return games;
	}

	public void setGames(Games games) {
		this.games = games;
	}

	public int getIdGameWinner() {
		return idGameWinner;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		GameWinner that = (GameWinner) o;
		return idGameWinner == that.idGameWinner;
	}

	@Override
	public int hashCode() {
		return Objects.hash(idGameWinner);
	}
}
