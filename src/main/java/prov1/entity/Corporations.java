package prov1.entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;

@Entity
@Table(name = "Corporations")
public class Corporations implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idCorporation", updatable = false, insertable = false)
	private int id;

	@Column(name = "name", length = 50, nullable = false)
	private String name;

	@Column(name = "description", length = 150, nullable = false)
	private String description;

	@Column(name = "victoryPoints")
	private int victoryPoints = 0;

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "player")
	@JsonManagedReference
	private Players player;

	@OneToMany(mappedBy = "corporations", cascade = CascadeType.ALL)
	@JsonBackReference
	private Set<Makers> listamakers = new HashSet<>();

	public Corporations() {
		super();
	}

	public Corporations(int id, String name, String description, int victoryPoints, Players player) {
		this.id = id;
		this.name = name;
		this.description = description;
		this.victoryPoints = victoryPoints;
		this.player = player;
	}

	public Corporations(String name, String description) {
		this.name = name;
		this.description = description;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		Corporations that = (Corporations) o;
		return id == that.id;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getVictoryPoints() {
		return victoryPoints;
	}

	public void setVictoryPoints(int victoryPoints) {
		this.victoryPoints = victoryPoints;
	}

	public Players getPlayer() {
		return player;
	}

	public void setPlayer(Players player) {
		this.player = player;
	}

	public Set<Makers> getListamakers() {
		return listamakers;
	}

	public void setListamakers(Set<Makers> listamakers) {
		this.listamakers = listamakers;
	}

	@Override
	public String toString() {
		return "Corporations [id=" + id + ", name=" + name + ", description=" + description + ", victoryPoints="
				+ victoryPoints + ", player=" + player + ", listamakers=" + listamakers + "]";
	}

}
