package prov1.entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

@Entity
@Table(name = "Makers")
public class Makers implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idMaker", updatable = false, insertable = false)
	private int id;

	@Column(name = "name", length = 20)
	private String name;

	@Column(name = "maxneighbours")
	private int maxneighbours = 0;

	@Enumerated(EnumType.STRING)
	@Column(name = "typemaker")
	private TypeMaker typemaker;

	@ManyToOne
	@JoinColumn(name = "idCorporation")
	@JsonManagedReference
	private Corporations corporations;

	@ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.REMOVE }, fetch = FetchType.EAGER)
	@JoinTable(name = "MakersNeighbours")
	@JsonManagedReference
	private Set<Makers> neighbours = new HashSet<>();

	/*
	 * @ManyToMany(cascade={CascadeType.PERSIST,CascadeType.REMOVE}, mappedBy =
	 * "neighbours", fetch = FetchType.EAGER)
	 * 
	 * @JsonBackReference private Set<Makers> me = new HashSet<>();
	 */
	public enum TypeMaker {
		CIUTAT, BOSC, OCEA
	}

	public Makers() {
		super();
	}

	public Makers(int id, String name, int maxneighbours, TypeMaker typemaker, Corporations corporations,
			Set<Makers> neighbours, Set<Makers> me) {

		super();
		this.id = id;
		this.name = name;
		this.maxneighbours = maxneighbours;
		this.typemaker = typemaker;
		this.corporations = corporations;
		this.neighbours = neighbours;
		// this.me = me;
	}

	public Makers(String name, int maxneighbours, TypeMaker typemaker) {
		super();
		this.name = name;
		this.maxneighbours = maxneighbours;
		this.typemaker = typemaker;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getMaxneighbours() {
		return maxneighbours;
	}

	public void setMaxneighbours(int maxneighbours) {
		this.maxneighbours = maxneighbours;
	}

	public TypeMaker getTypemaker() {
		return typemaker;
	}

	public void setTypemaker(TypeMaker typemaker) {
		this.typemaker = typemaker;
	}

	public Corporations getCorporations() {
		return corporations;
	}

	public void setCorporations(Corporations corporations) {
		this.corporations = corporations;
	}

	public Set<Makers> getNeighbours() {
		return neighbours;
	}

	public void setNeighbours(Set<Makers> neighbours) {
		this.neighbours = neighbours;
	}

	/*
	 * public Set<Makers> getMe() { return me; }
	 * 
	 * public void setMe(Set<Makers> me) { this.me = me; }
	 */
	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		Makers makers = (Makers) o;
		return id == makers.id;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	public int getId() {
		return id;
	}
}
