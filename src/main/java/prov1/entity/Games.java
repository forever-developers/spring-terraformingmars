package prov1.entity;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;

@Entity
@Table(name = "Games")
public class Games {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idGame", updatable = false, insertable = false)
	private int idGame;

	@Column(name = "oxigen")
	private int oxigen = 0;

	@Column(name = "temperature")
	private int temperature = -30;

	@Column(name = "oceans")
	private int oceans = 0;

	@Column(name = "datestart")
	private LocalDateTime datestart = LocalDateTime.now();

	@Column(name = "dateend")
	private LocalDateTime dateend = LocalDateTime.now();

	@ManyToMany(cascade = { CascadeType.ALL })
	@JoinTable(name = "GamesPlayers")
	@JsonManagedReference
	private Set<Players> players = new HashSet<Players>();

	@OneToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(name = "idGameWinner")
	@JsonManagedReference
	private GameWinner gameWinner;

	public Games() {
		super();
	}

	public Games(int idGame, int oxigen, int temperature, int oceans, LocalDateTime datestart, LocalDateTime dateend) {
		super();
		this.idGame = idGame;
		this.oxigen = oxigen;
		this.temperature = temperature;
		this.oceans = oceans;
		this.datestart = datestart;
		this.dateend = dateend;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		Games games = (Games) o;
		return idGame == games.idGame;
	}

	@Override
	public int hashCode() {
		return Objects.hash(idGame);
	}

	public int getIdGame() {
		return idGame;
	}

	public int getOxigen() {
		return oxigen;
	}

	public void setOxigen(int oxigen) {
		this.oxigen = oxigen;
	}

	public int getTemperature() {
		return temperature;
	}

	public void setTemperature(int temperature) {
		this.temperature = temperature;
	}

	public int getOceans() {
		return oceans;
	}

	public void setOceans(int oceans) {
		this.oceans = oceans;
	}

	public LocalDateTime getDatestart() {
		return datestart;
	}

	public void setDatestart(LocalDateTime datestart) {
		this.datestart = datestart;
	}

	public LocalDateTime getDateend() {
		return dateend;
	}

	public void setDateend(LocalDateTime dateend) {
		this.dateend = dateend;
	}

	public Set<Players> getPlayers() {
		return players;
	}

	public void setPlayers(Set<Players> players) {
		this.players = players;
	}

	public GameWinner getGameWinner() {
		return gameWinner;
	}

	public void setGameWinner(GameWinner gameWinner) {
		this.gameWinner = gameWinner;
	}
}
