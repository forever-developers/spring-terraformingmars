package prov1.entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;

@Entity
@Table(name = "Players")
public class Players implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idPlayer", updatable = false, insertable = false)
	private int id;

	@Column(name = "name", length = 50, nullable = false)
	private String name;

	@Column(name = "wins")
	private int wins = 0;

	@OneToOne(cascade = CascadeType.ALL, mappedBy = "player", fetch = FetchType.EAGER)
	@JsonBackReference
	private Corporations corporation;

	@OneToMany(mappedBy = "players", cascade = CascadeType.ALL)
	@JsonBackReference
	private Set<GameWinner> gameWinners = new HashSet<GameWinner>();

//    @ManyToMany(cascade = {CascadeType.ALL}, mappedBy = "players")
//    @JsonBackReference
	// private Set<Games> games = new HashSet<Games>();

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		Players players = (Players) o;
		return id == players.id;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	public Players(int id, String name, int wins, Corporations corporation, Set<GameWinner> gameWinners,
			Set<Games> games) {
		this.id = id;
		this.name = name;
		this.wins = wins;
		this.corporation = corporation;
		this.gameWinners = gameWinners;
		// this.games = games;
	}

	public Players() {
	}

	public Players(String name) {
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getWins() {
		return wins;
	}

	public void setWins(int wins) {
		this.wins = wins;
	}

	public Corporations getCorporation() {
		return corporation;
	}

	public void setCorporation(Corporations corporation) {
		this.corporation = corporation;
	}

	public Set<GameWinner> getGameWinners() {
		return gameWinners;
	}

	public void setGameWinners(Set<GameWinner> gameWinners) {
		this.gameWinners = gameWinners;
	}

	/*
	 * public Set<Games> getGames() { return games; }
	 * 
	 * public void setGames(Set<Games> games) { this.games = games; }
	 */
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Players [id=" + id + ", name=" + name + ", wins=" + wins + ", corporation=" + corporation
				+ ", gameWinners=" + gameWinners + "]";
	}

}
