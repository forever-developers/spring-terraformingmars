package prov1.entity;

import java.io.Serializable;
import java.util.Arrays;

public class Dices implements Serializable{
	private int[] dices;
	private int corporation;

	public Dices() {
		super();
	}

	public Dices(int[] dices) {
		super();
		this.dices = dices;
	}
	

	public Dices(int[] dices, int corporation) {
		this(dices);
		this.corporation = corporation;
	}

	public int[] getDices() {
		return dices;
	}

	public void setDices(int[] dices) {
		this.dices = dices;
	}

	public int getCorporation() {
		return corporation;
	}

	public void setCorporation(int corporation) {
		this.corporation = corporation;
	}

	@Override
	public String toString() {
		return "Dices [dices=" + Arrays.toString(dices) + ", corporation=" + corporation + "]";
	}
	
}
