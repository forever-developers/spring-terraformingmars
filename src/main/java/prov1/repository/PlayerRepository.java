package prov1.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import prov1.entity.Corporations;
import prov1.entity.Games;
import prov1.entity.Players;

public interface PlayerRepository extends JpaRepository<Players, Integer> {

	List<Players> findAll();

	List<Players> findByNameIgnoreCase(String name);

	List<Players> findByWins(int wins);

	List<Players> findByCorporation(Corporations corporations);
	// List<Players> findByGames(Games games);

}
