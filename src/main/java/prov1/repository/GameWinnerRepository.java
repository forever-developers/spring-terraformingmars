package prov1.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import prov1.entity.GameWinner;
import prov1.entity.Games;
import prov1.entity.Players;

public interface GameWinnerRepository extends JpaRepository<GameWinner, Integer> {

	List<GameWinner> findByPlayers(Players players);

	List<GameWinner> findByGames(Games games);

}
