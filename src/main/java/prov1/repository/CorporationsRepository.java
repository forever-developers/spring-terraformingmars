package prov1.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import prov1.entity.Corporations;
import prov1.entity.Makers;
import prov1.entity.Players;

public interface CorporationsRepository extends JpaRepository<Corporations, Integer> {

	List<Corporations> findByListamakers(Makers makers);

	List<Corporations> findByPlayerIsNotNull();

}
