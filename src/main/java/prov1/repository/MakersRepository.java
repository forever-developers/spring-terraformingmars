package prov1.repository;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;

import prov1.entity.Makers;
import prov1.entity.Makers.TypeMaker;

public interface MakersRepository extends JpaRepository<Makers, Integer> {

	List<Makers> findByNameIgnoreCase(String name);

	List<Makers> findByMaxneighbours(int maxneighbours);

	List<Makers> findByTypemaker(TypeMaker typemaker);

	List<Makers> findByNeighboursIn(Set<Makers> neighbours);

	List<Makers> findByCorporationsIsNullAndTypemaker(TypeMaker typemaker);

	List<Makers> findByCorporationsIsNotNullAndTypemaker(TypeMaker typemaker);

}
