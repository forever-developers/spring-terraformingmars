package prov1.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import prov1.entity.GameWinner;
import prov1.entity.Games;
import prov1.entity.Players;

public interface GamesRepository extends JpaRepository<Games, Integer> {

	List<Games> findByOxigen(int oxigen);

	List<Games> findByTemperature(int temperature);

	List<Games> findByOceans(int oceans);

	List<Games> findByPlayers(Players players);

	List<Games> findByGameWinner(GameWinner gamewinner);

}
