package prov1.controller;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import jakarta.servlet.http.HttpServletRequest;
import prov1.entity.Corporations;
import prov1.entity.Dices;
import prov1.entity.GameWinner;
import prov1.entity.Games;
import prov1.entity.Makers;
import prov1.entity.Makers.TypeMaker;
import prov1.entity.Players;
import prov1.entity.Resultat;
import prov1.services.CorporationsService;
import prov1.services.GameWinnerService;
import prov1.services.GamesService;
import prov1.services.MakersService;
import prov1.services.PlayersService;
import prov1.utils.ReturnOfJedy;

@RestController
public class ApiControllerTerraforming {

	@Autowired
	CorporationsService corpService;

	@Autowired
	GamesService gameService;

	@Autowired
	GameWinnerService gameWinnerService;

	@Autowired
	MakersService makerService;

	@Autowired
	PlayersService playerService;

	@GetMapping(path = "/alive")
	public ResponseEntity<?> alive() {
		return ResponseEntity.ok(new ReturnOfJedy("Hello"));
	}

	@GetMapping(path = "/getCorporationsWithPlayer")
	public ResponseEntity<?> corporatationsWhitPlayers() {
		return ResponseEntity.ok(corpService.findByPlayersIsNotNull());
	}

	@GetMapping(path = "/getMakersByTypeWithoutCorporation")
	public ResponseEntity<?> makersNoCorporation(@RequestParam TypeMaker typemaker) {
		return ResponseEntity.ok(makerService.findByCorporationsIsNull(typemaker));
	}

	@GetMapping(path = "/getMakersByTypeWithCorporation")
	public ResponseEntity<?> makersCorporation(@RequestParam TypeMaker typemaker) {
		return ResponseEntity.ok(makerService.findByCorporationsIsNotNull(typemaker));
	}

	Random r = new Random();

	@GetMapping(path = "/rollingDices")
	public ResponseEntity<?> dices() {

		int[] dices = { r.nextInt(6), r.nextInt(6), r.nextInt(6), r.nextInt(6), r.nextInt(6), r.nextInt(6) };
		Dices d = new Dices(dices);
		return ResponseEntity.ok(d);
	}

	@PostMapping(path = "/resolveDices")
	public ResponseEntity<?> resolveDice(HttpServletRequest request, Dices dice) throws IOException {
		
		//Stream<String> list = request.getReader().lines().collect(null) ;
		
//		System.out.println(request.getReader().lines().collect(Collectors.joining(System.lineSeparator())));
//		String text = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
		
		List<Integer> integers = extractIntegersFromString(request.getReader().lines().collect(Collectors.joining(System.lineSeparator())));

        
		int[] array = new int[6];
				
		for (int i = 1; i < integers.size(); i++) {
			array[i-1] = integers.get(i);
		}
		
		Dices dices = new Dices(array, integers.get(0));
		System.out.println(dices);
		
		//System.out.println(request.getAttributeNames() );
		return ResponseEntity.ok(resolveDices(dices));
	}

	private static List<Integer> extractIntegersFromString(String input) {
        List<Integer> integers = new ArrayList<>();
        String regex = "\\b\\d+\\b"; // Expresión regular para encontrar números enteros

        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(input);

        while (matcher.find()) {
            integers.add(Integer.parseInt(matcher.group()));
        }

        return integers;
    }
	@GetMapping(path = "/isEndGame")
	public ResponseEntity<?> isEndGame() {
		return ResponseEntity.ok(isEndedGame());
	}

	@PostMapping(path = "/setVictoryPointsWinners")
	public ResponseEntity<?> setVictoryPointsWinners() {
		return ResponseEntity.ok(setVictoryPoints());
	}

	@PostMapping(path = "/setVictoryPointsMakers")
	public ResponseEntity<?> setVictoryPointsByTypeMaker() {
		return ResponseEntity.ok(setVictoryPointsByType());
	}

	@PostMapping(path = "/setWinnerGame")
	public ResponseEntity<?> setWinner() {
		return ResponseEntity.ok(setWinnerGame());
	}

	public Resultat setVictoryPoints() {
		Resultat res = new Resultat();
		List<Players> listaPlayer = playerService.findAll();
		for (Players player : listaPlayer) {
			player.getCorporation().setVictoryPoints(
					player.getCorporation().getVictoryPoints() + player.getCorporation().getListamakers().size());
			res.setEstat(res.getEstat() + "/***/ Se le asigna a " + player.getName() + " "
					+ player.getCorporation().getListamakers().size() + " puntos por casillas en el tablero\n");
		}
		return res;
	}

	public Resultat resolveDices(Dices dices) {

		System.out.println("*******************");
		System.out.println(dices);
		System.out.println("*******************");
		Resultat res = new Resultat();
		res.setEstat("/***/");
		Corporations corp = corpService.findById(dices.getCorporation());
		Games currentGame = gameService.findById(1);
		int countTemp = 0;
		int countOxygen = 0;
		int countOcean = 0;
		int countForest = 0;
		int countCity = 0;
		int countPoints = 0;

		for (int dicenumber : dices.getDices()) {

			if (dicenumber == 0)
				countTemp++;
			else if (dicenumber == 1)
				countOxygen++;
			else if (dicenumber == 2)
				countOcean++;
			else if (dicenumber == 3)
				countForest++;
			else if (dicenumber == 4)
				countCity++;
			else if (dicenumber == 5)
				countPoints++;
		}
		
		System.out.println("OCEANO: "+countOcean);

		if (countTemp >= 3) {

			currentGame.setTemperature(currentGame.getTemperature() + 2);
			gameService.save(currentGame);
			res.setEstat(res.getEstat() + "\n /***/ Ha aumentado la temperatura, ahora tiene un valor de "
					+ currentGame.getTemperature());

		}

		if (countOxygen >= 3) {

			if (currentGame.getOxigen() < 14) {
				currentGame.setOxigen(currentGame.getOxigen() + 1);
				gameService.save(currentGame);

				res.setEstat(res.getEstat() + "\n /***/ Ha aumentado el oxigeno, ahora tiene un valor de "
						+ currentGame.getOxigen());
			} else {
				res.setEstat(res.getEstat() + "\n /***/ Has llegado al maximo de oxigeno");
			}

		}

		if (countPoints >= 3) {
			corp.setVictoryPoints(corp.getVictoryPoints() + 2);
			res.setEstat(res.getEstat() + "\n /***/ Ha aumentado los puntos de victoria de la corporacion "
					+ corp.getName() + " perteneciente a " + corp.getPlayer().getName() + ", ahora tienen un valor de "
					+ corp.getVictoryPoints());
		}

		if (countOcean >= 3) {

			ArrayList<Makers> oceanMakers = (ArrayList<Makers>) makerService
					.findByCorporationsIsNull(Makers.TypeMaker.OCEA);

			if (!oceanMakers.isEmpty()) {

				res.setEstat(res.getEstat() + "\n /***/ Se ha asignado una casilla de OCEANO a la corporacion "
						+ corp.getName() + " perteneciente a " + corp.getPlayer().getName());
				Makers oceanMaker = oceanMakers.get(r.nextInt(oceanMakers.size()));
				corp.getListamakers().add(oceanMaker);
				oceanMaker.setCorporations(corp);
				makerService.save(oceanMaker);

			} else {
				res.setEstat(res.getEstat() + "\n /***/ No hay mas casillas de OCEANO para asignar");
			}
		}

		if (countCity >= 3) {

			ArrayList<Makers> citiesMakers = (ArrayList<Makers>) makerService
					.findByCorporationsIsNull(Makers.TypeMaker.CIUTAT);

			if (!citiesMakers.isEmpty()) {

				System.out.println("/***/ Se ha asignado una casilla de CIUDAD a la corporacion " + corp.getName()
						+ " perteneciente a " + corp.getPlayer().getName());
				Makers cityMakers = citiesMakers.get(r.nextInt(citiesMakers.size()));
				corp.getListamakers().add(cityMakers);
				cityMakers.setCorporations(corp);
				makerService.save(cityMakers);

			} else {
				res.setEstat(res.getEstat() + "\n /***/ No hay mas casillas de CIUDAD para asignar");
			}
		}

		if (countForest >= 4) {

			ArrayList<Makers> forestMakers = (ArrayList<Makers>) makerService
					.findByCorporationsIsNull(Makers.TypeMaker.BOSC);

			if (!forestMakers.isEmpty()) {

				res.setEstat(res.getEstat() + "\n /***/ Se ha asignado una casilla de BOSQUE a la corporacion "
						+ corp.getName() + " perteneciente a " + corp.getPlayer().getName());
				Makers forestMaker = forestMakers.get(r.nextInt(forestMakers.size()));
				corp.getListamakers().add(forestMaker);
				forestMaker.setCorporations(corp);
				makerService.save(forestMaker);

			} else {
				res.setEstat(res.getEstat() + "\n /***/ No hay mas casillas de BOSQUE para asignar");

			}
		}

		corpService.save(corp);

		countTemp = 0;
		countOxygen = 0;
		countOcean = 0;
		countForest = 0;
		countCity = 0;
		countPoints = 0;

		return res;
	}

	public Resultat isEndedGame() {
		Games currentGame = gameService.findById(1);
		Resultat res = new Resultat();
		int count = 0;

		if (currentGame.getOxigen() >= 14)
			count++;

		if (currentGame.getTemperature() >= 0)
			count++;

		ArrayList<Makers> oceanMakers = (ArrayList<Makers>) makerService
				.findByCorporationsIsNull(Makers.TypeMaker.OCEA);
		if (oceanMakers.size() <= 0)
			count++;

		if (count >= 2) {
			res.setEstat("Se acabo la partida!");

		} else
			res.setEstat("La partida continua!");

		return res;
	}

	public Resultat setWinnerGame() {
		List<Players> playerList = playerService.findAll();
		GameWinner winGame = gameWinnerService.findById(1);
		Games currentGame = gameService.findById(1);
		Resultat res = new Resultat();

		int points = -1;
		Players playerWin = null;
		Players playerEmpate = null;
		boolean empate = false;

		for (int i = 0; i < playerList.size(); i++) {

			if (playerList.get(i).getCorporation().getVictoryPoints() == points) {
				empate = true;
				points = playerList.get(i).getCorporation().getVictoryPoints();
				playerEmpate = playerList.get(i);
			}
			if (playerList.get(i).getCorporation().getVictoryPoints() > points) {
				empate = false;
				playerWin = playerList.get(i);
			}

		}

		if (playerEmpate == null)
			res.setEstat("Ha ganado " + playerWin.getName() + " con un total de "
					+ playerWin.getCorporation().getVictoryPoints() + " puntos");
		else
			res.setEstat("Se ha empatado la partida entre " + playerWin.getName() + " y " + playerEmpate.getName()
					+ " con un total de " + playerWin.getCorporation().getVictoryPoints() + " puntos");

		winGame.setPlayers(playerWin);
		currentGame.setDateend(LocalDateTime.now());
		gameService.save(currentGame);
		gameWinnerService.save(winGame);

		return res;

	}

	public Resultat setVictoryPointsByType() {

		Resultat res = new Resultat();
		res.setEstat("/***/ ");

		List<Players> listPlayers = playerService.findAll();

		Map<Players, Integer> puntosCity = new HashMap<>();
		Map<Players, Integer> puntosForest = new HashMap<>();

		for (Players player : listPlayers) {

			if (checkOceansPlayer(player)) {

				player.getCorporation().setVictoryPoints(player.getCorporation().getVictoryPoints() + 3);
				res.setEstat(res.getEstat() + "/***/ Se le asignan 3 puntos a " + player.getName()
						+ " por tener al menos 3 casillas de tipo OCEANO");
				;
			}

			puntosCity.put(player, checkCityPlayer(player));
			puntosForest.put(player, checkForestPlayer(player));

		}

		// PUNTOS BY CITY

		Players mejorJugador = null;
		int maxPuntos = Integer.MIN_VALUE;

		for (Map.Entry<Players, Integer> entry : puntosCity.entrySet()) {
			Players jugador = entry.getKey();
			int puntos = entry.getValue();

			if (puntos > maxPuntos) {
				maxPuntos = puntos;
				mejorJugador = jugador;
			}
		}

		if (maxPuntos != 0) {
			mejorJugador.getCorporation().setVictoryPoints(mejorJugador.getCorporation().getVictoryPoints() + 5);
			res.setEstat(res.getEstat() + "/***/ El jugador " + mejorJugador.getName()
					+ " consige 5 puntos por tener mas ciudades que el resto (" + maxPuntos + ")");
		}

		Players segundoMejorJugador = null;
		int segundoMaxPuntos = Integer.MIN_VALUE;

		for (Map.Entry<Players, Integer> entry : puntosCity.entrySet()) {
			Players jugador = entry.getKey();
			int puntos = entry.getValue();

			if (!jugador.equals(mejorJugador) && puntos > segundoMaxPuntos) {
				segundoMaxPuntos = puntos;
				segundoMejorJugador = jugador;
			}
		}
		if (segundoMaxPuntos != 0) {
			segundoMejorJugador.getCorporation()
					.setVictoryPoints(segundoMejorJugador.getCorporation().getVictoryPoints() + 3);
			res.setEstat(res.getEstat() + "/***/ El jugador " + segundoMejorJugador.getName()
					+ " consige 3 puntos por ser el segundo en tener mas ciudades que el resto (" + segundoMaxPuntos
					+ ")");
		}

		// PUNTOS BY FOREST

		Players mejorJugadorForest = null;
		int maxPuntosForest = Integer.MIN_VALUE;

		for (Map.Entry<Players, Integer> entry : puntosForest.entrySet()) {
			Players jugador = entry.getKey();
			int puntos = entry.getValue();

			if (puntos > maxPuntosForest) {
				maxPuntosForest = puntos;
				mejorJugadorForest = jugador;
			}
		}
		if (maxPuntosForest != 0) {
			mejorJugadorForest.getCorporation()
					.setVictoryPoints(mejorJugadorForest.getCorporation().getVictoryPoints() + 5);
			res.setEstat(res.getEstat() + "/***/ El jugador " + mejorJugadorForest.getName()
					+ " consige 5 puntos por tener mas bosques que el resto (" + maxPuntosForest + ")");
		}

		Players segundoMejorJugadorForest = null;
		int segundoMaxPuntosForest = Integer.MIN_VALUE;

		for (Map.Entry<Players, Integer> entry : puntosForest.entrySet()) {
			Players jugador = entry.getKey();
			int puntos = entry.getValue();

			if (!jugador.equals(mejorJugadorForest) && puntos > segundoMaxPuntosForest) {
				segundoMaxPuntosForest = puntos;
				segundoMejorJugadorForest = jugador;
			}
		}
		if (segundoMaxPuntosForest != 0) {
			segundoMejorJugadorForest.getCorporation()
					.setVictoryPoints(segundoMejorJugadorForest.getCorporation().getVictoryPoints() + 3);
			res.setEstat(res.getEstat() + "/***/ El jugador " + segundoMejorJugador.getName()
					+ " consige 3 puntos por ser el segundo en tener mas bosques que el resto ("
					+ segundoMaxPuntosForest + ")");
			;
		}

		for (Players player : listPlayers) {

			playerService.save(player);
		}

		return res;

	}

	private boolean checkOceansPlayer(Players p) {
		int oceanPlayerCount = 0;
		for (Makers maker : p.getCorporation().getListamakers()) {

			if (maker.getTypemaker() == Makers.TypeMaker.OCEA) {
				oceanPlayerCount++;
			}
		}
		return oceanPlayerCount >= 3;
	}

	private int checkCityPlayer(Players p) {
		int cityPlayerCount = 0;
		for (Makers maker : p.getCorporation().getListamakers()) {

			if (maker.getTypemaker() == Makers.TypeMaker.CIUTAT) {
				cityPlayerCount++;
			}
		}
		return cityPlayerCount;
	}

	private int checkForestPlayer(Players p) {
		int forestPlayerCount = 0;
		for (Makers maker : p.getCorporation().getListamakers()) {

			if (maker.getTypemaker() == Makers.TypeMaker.BOSC) {
				forestPlayerCount++;
			}
		}
		return forestPlayerCount;
	}

}
