package prov1.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import prov1.entity.GameWinner;
import prov1.entity.Games;
import prov1.entity.Players;
import prov1.repository.GameWinnerRepository;
import prov1.repository.PlayerRepository;

@Service
public class GameWinnerService {

	@Autowired
	GameWinnerRepository repositori;

	public GameWinner findById(Integer id) {
		return repositori.findById(id).orElse(null);
	}

	public List<GameWinner> findAll() {
		return repositori.findAll();
	}

	public void delete(Integer id) {

		repositori.deleteById(id);
	}

	public void deleteAll() {

		repositori.deleteAll();
	}

	public GameWinner save(GameWinner gameWinner) {
		return repositori.save(gameWinner);
	}

	public List<GameWinner> findByPlayers(Players players) {
		return repositori.findByPlayers(players);
	}

	public List<GameWinner> findByGames(Games games) {
		return repositori.findByGames(games);
	}

}
