package prov1.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import prov1.entity.GameWinner;
import prov1.entity.Games;
import prov1.entity.Players;
import prov1.repository.GamesRepository;
import prov1.repository.PlayerRepository;

@Service
public class GamesService {

	@Autowired
	GamesRepository repositori;

	public Games findById(Integer id) {
		return repositori.findById(id).orElse(null);
	}

	public List<Games> findAll() {
		return repositori.findAll();
	}

	public void delete(Integer id) {

		repositori.deleteById(id);
	}

	public void deleteAll() {

		repositori.deleteAll();
	}

	public Games save(Games games) {
		return repositori.save(games);
	}

	public List<Games> findByOxigen(int oxigen) {
		return repositori.findByOxigen(oxigen);
	}

	public List<Games> findByTemperature(int temperature) {
		return repositori.findByTemperature(temperature);
	}

	public List<Games> findByOceans(int oceans) {
		return repositori.findByOceans(oceans);
	}

	public List<Games> findByPlayers(Players players) {
		return repositori.findByPlayers(players);
	}

	public List<Games> findByGameWinner(GameWinner gamewinner) {
		return repositori.findByGameWinner(gamewinner);
	}

}
