package prov1.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import prov1.entity.Corporations;
import prov1.entity.Makers;
import prov1.entity.Makers.TypeMaker;
import prov1.entity.Players;
import prov1.repository.CorporationsRepository;

@Service
public class CorporationsService {

	@Autowired
	CorporationsRepository repositori;

	public Corporations findById(Integer id) {
		return repositori.findById(id).orElse(null);
	}

	public List<Corporations> findAll() {
		return repositori.findAll();
	}

	public void delete(Integer id) {

		repositori.deleteById(id);
	}

	public void deleteAll() {

		repositori.deleteAll();
	}

	public Corporations save(Corporations corporations) {
		return repositori.save(corporations);
	}

	public List<Corporations> findByListmakers(Makers makers) {
		return repositori.findByListamakers(makers);
	}

	public List<Corporations> findByPlayersIsNotNull() {
		return repositori.findByPlayerIsNotNull();
	}

}
