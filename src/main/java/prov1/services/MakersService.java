package prov1.services;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import prov1.entity.Makers;
import prov1.entity.Players;
import prov1.entity.Makers.TypeMaker;
import prov1.repository.MakersRepository;

@Service
public class MakersService {

	@Autowired
	MakersRepository repositori;

	public Makers findById(Integer id) {
		return repositori.findById(id).orElse(null);
	}

	public List<Makers> findAll() {
		return repositori.findAll();
	}

	public void delete(Integer id) {

		repositori.deleteById(id);
	}

	public void deleteAll() {

		repositori.deleteAll();
	}

	public Makers save(Makers makers) {
		return repositori.save(makers);
	}

	public List<Makers> findByName(String name) {
		return repositori.findByNameIgnoreCase(name);
	}

	public List<Makers> findByMaxNeighbours(int maxneighbours) {
		return repositori.findByMaxneighbours(maxneighbours);
	}

	public List<Makers> findByTypeMaker(TypeMaker typemaker) {
		return repositori.findByTypemaker(typemaker);
	}

	public List<Makers> findByMakersNeighbours(Set<Makers> neighbours) {
		return repositori.findByNeighboursIn(neighbours);
	}

	public List<Makers> findByCorporationsIsNull(TypeMaker typemaker) {
		return repositori.findByCorporationsIsNullAndTypemaker(typemaker);
	}

	public List<Makers> findByCorporationsIsNotNull(TypeMaker typemaker) {
		return repositori.findByCorporationsIsNotNullAndTypemaker(typemaker);
	}

}
