package prov1.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import prov1.entity.Corporations;
import prov1.entity.Games;
import prov1.entity.Players;
import prov1.repository.PlayerRepository;

@Service
public class PlayersService {

	@Autowired
	PlayerRepository repositori;

	public Players findById(Integer id) {
		return repositori.findById(id).orElse(null);
	}

	public List<Players> findAll() {
		return repositori.findAll();
	}

	public void delete(Integer id) {

		repositori.deleteById(id);
	}

	public void deleteAll() {

		repositori.deleteAll();
	}

	public Players save(Players player) {
		return repositori.save(player);
	}

	public List<Players> findByName(String name) {
		return repositori.findByNameIgnoreCase(name);
	}

	public List<Players> findByWins(int wins) {
		return repositori.findByWins(wins);
	}

	public List<Players> findByCorporations(Corporations corporations) {
		return repositori.findByCorporation(corporations);
	}

	/*
	 * public List<Players> findByGames(Games games){ return
	 * repositori.findByGames(games); }
	 */

}
